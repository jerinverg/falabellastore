package com.falabella.store.model;

import org.springframework.data.annotation.Id;


public class Product {


	@Id
	private int id;
	private String sku;
	private String name;
	private String brand;
	private String size;
	private long price;
	private String imageURLs;

	
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Product(String sku, String name, String brand, String size, long price, String imageURLs) {
		super();
		this.sku = sku;
		this.name = name;
		this.brand = brand;
		this.size = size;
		this.price = price;
		this.imageURLs = imageURLs;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public String getImageURLs() {
		return imageURLs;
	}

	public void setImageURLs(String imageURLs) {
		this.imageURLs = imageURLs;
	}

}
