package com.falabella.store.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.falabella.store.dao.ProductDao;
import com.falabella.store.model.Product;

@Service
public class StoreServiceImpl implements StoreService {

	@Autowired
	ProductDao productDao;

	public void doSave(Product product) {

		productDao.createProduct(product);

	}

	public Product getProductbyId(Product product) {

		return productDao.fetchProductById(product.getSku());

	}
	
	public List<Product> getAllProducts(){
		return productDao.getAllProducts();
	}
	
	public void deleteProduct(Product product) {
		productDao.deleteProductsById(product.getSku()); 
	}

}
