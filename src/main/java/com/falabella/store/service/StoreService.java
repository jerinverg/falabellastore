package com.falabella.store.service;

import java.util.List;

import com.falabella.store.model.Product;

public interface StoreService {

	void doSave(Product product);

	public Product getProductbyId(Product product);

	public List<Product> getAllProducts();

	public void deleteProduct(Product product);

}
