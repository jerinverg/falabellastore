package com.falabella.store.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.falabella.store.dao.ProductDao;
import com.falabella.store.model.Product;

public class ProductDaoImpl implements ProductDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	@Override
	public void createProduct(Product product) {
		String SQL = "INSERT INTO product (sku, name, brand, size, price ) VALUES (?,?,?,?,?)";
		int update = getJdbcTemplate().update(SQL, product.getSku(),product.getName(),product.getBrand(),product.getSize(), product.getPrice());
		if(update == 1) {
			System.out.println("Product is created..");
		}
	}
	
	@Override
	public Product fetchProductById(String sku) {
		System.out.println(getJdbcTemplate().getDataSource().getClass().getName());
		String SQL = "SELECT * FROM product WHERE sku = ?";
		return getJdbcTemplate().queryForObject(SQL, new ProductRowMapper(), sku);
	}
	@Override
	public List<Product> getAllProducts() {
		String SQL = "SELECT * FROM product";
		return getJdbcTemplate().query(SQL, new ProductRowMapper());
	}
	
	@Override
	public void deleteProductsById(String sku) {
		String SQL = "DELETE FROM product WHERE sku = ?";
		int update = getJdbcTemplate().update(SQL, sku);
		if(update == 1) {
			System.out.println("Product is deleted with ID = "+sku);
		}
	}

}
