package com.falabella.store.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.falabella.store.model.Product;

public class ProductRowMapper implements RowMapper<Product> {
	
	@Override
	public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
		Product product = new Product();
		product.setId(rs.getInt("idproduct"));
		product.setSku(rs.getString("ska"));
		product.setName(rs.getString("name"));
		product.setBrand(rs.getString("brand"));
		product.setSize(rs.getString("size"));
		product.setPrice(rs.getLong("price"));
		return product;
	}

}
