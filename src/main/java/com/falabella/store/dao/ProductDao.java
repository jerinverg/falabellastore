package com.falabella.store.dao;

import java.util.List;

import com.falabella.store.model.Product;

public interface ProductDao {

	public void createProduct(Product product);
	
	public Product fetchProductById(String sku);
	
	public List<Product> getAllProducts(); 
	
	public void deleteProductsById(String sku);

}
