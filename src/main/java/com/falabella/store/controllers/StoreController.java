package com.falabella.store.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.falabella.store.model.Product;
import com.falabella.store.service.StoreService;

@RestController
@RequestMapping("/store")
public class StoreController {

	@Autowired
	StoreService storeServices;

	@GetMapping("/products")
	public List<Product> getAllProducts() {
		
		return storeServices.getAllProducts();
	}

	@PostMapping("/saveProduct")
	public void doSaveProduct(@RequestBody Product product) {
		storeServices.doSave(product);
	}

}
