### Products RESTful API

Este Proyecto fue creado con Spring Boot y Maven. 
Para compilarlo debemos eejcutar el comando mv clean Install.
esto no exportara en la carpeta target del aplicativo un WAR donde podemos desplegarlo es nuestro servidor.
de igual forma por ser un proyecto spring boot se puede ejecutar con el boot DashBoard.


### Arquitectura

Se utilizo la version java 11
compilamos el proyecto con Maven
Usamos modelo MVC
y para la persistencia usamos JDBC y HikariCP

para la BD usamos MySQL se creo una tabla llamada product

CREATE TABLE `product` (
  `idproduct` int NOT NULL AUTO_INCREMENT,
  `sku` varchar(200) NOT NULL,
  `name` varchar(50) NOT NULL,
  `brand` varchar(50) NOT NULL,
  `size` varchar(50) DEFAULT NULL,
  `price` float NOT NULL,
  PRIMARY KEY (`idproduct`),
  UNIQUE KEY `idproduct_UNIQUE` (`idproduct`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci